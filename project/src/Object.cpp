#include "../include/Object.h"

#include <iostream>
#include <SFML/Graphics.hpp>

Object::Object(int x, int y, std::string filename) {
    this->init_texture(filename);
	this->init_sprite(x, y);
}

void Object::init_texture(std::string filename) {
    if (!this->texture.loadFromFile("../project/src/pics/" + filename)) {
        std::cout<< "ERROR";
    }
    texture.setSmooth(true);
}

void Object::init_sprite(int x, int y) {
    this->sprite.setTexture(this->texture);
    this->sprite.setPosition(x, y);
    this->sprite.scale(0.22f, 0.22f);
}

void Object::update (float timer) {
    //rectangle.move(0, 20*timer);
    
    sprite.move(0, 40*timer);
}

void Object::render(sf::RenderWindow* window) {
    //window->draw(rectangle);
    
    window->draw(sprite);
}

int Object::get_position_y() {
    //return rectangle.getPosition().y;

    return sprite.getPosition().y;
}

void Object::set_position(int x, int y) {
    //rectangle.setPosition(x, y);

    sprite.setPosition(x, y);
}
