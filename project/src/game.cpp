#include "../include/game.h"

void Game::init_window() {
    window = new sf::RenderWindow(sf::VideoMode(1923, 1081), "game");
}

void Game::update() {
    update_SFML_events();

    if (!states.empty()) {
        states.top()->update(sound, sound_true);

        if (states.top()->get_quit_game()) {
            window->close();
        }
    } else {
        window->close();
    }

}

//void Game::update_time() {
//    time = clock.getElapsedTime().asSeconds();
//    clock.restart();
//    timer += time;
//}

void Game::render() {
    window->clear();
    if (!states.empty()) {
        states.top()->render();
    }

    window->display();
}

void Game::run() {

    std::ifstream file_path ("../config.txt");

    if (!file_path) {
        throw ("ERROR::COULDN'T LOAD MUSIC PATH");
    }

    std::string s, str;

    getline(file_path,s);

    str = "../" + s;

    file_path.close();
    sf::SoundBuffer buffer;
    buffer.loadFromFile(str);
    sound.setBuffer(buffer);
    buffer.loadFromFile(str);
    sound.setBuffer(buffer);
    sound_true.setBuffer(buffer);
    //sound.play();
    while(window->isOpen()) {
        //update_time();
        update();
        render();
    }
}

Game::Game() {
    init_window();
    //init_time();
    init_states();
}

Game::~Game() {
    delete window;

    while(!states.empty()) {
        delete states.top();
        states.pop();
    }
}

void Game::update_SFML_events() {
    while (window->pollEvent(sf_event)) {
        if (sf_event.type == sf::Event::Closed) {
            window->close();
        }
    }
}

void Game::init_states() {
  //  states.push(new State_Game(window, states));
    states.push(new State_Menu(window, &states));
}
