#ifndef BEAT_SABER_BUTTON_H
#define BEAT_SABER_BUTTON_H

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <sstream>
#include <unistd.h>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>

enum state_button {
    BTN_IDLE = 0,
    BTN_HOVER,
    BTN_ACTIVE,
    BTN_CUBE
};

class Button {
private:
    short unsigned button_state;

    sf::RectangleShape button;
    sf::Font* font;
    sf::Text text;

    sf::Texture texture;

    sf::Color idle_color;
    sf::Color hover_color;
    sf::Color active_color;

public:
    Button(float x, float y, float width, float height,
           sf::Font* font, std::string text,
           sf::Color idle_color, sf::Color hover_color, sf::Color active_color);

    Button(float x, float y, float width, float height,
           sf::Color idle_color, sf::Color hover_color, sf::Color active_color);

    ~Button() = default;

    const bool is_pressed() const;

    void update(const sf::Vector2f& mouse_pos);

    void update(bool cube_on_btn, int);

    void render(sf::RenderWindow* window);
};

#endif //BEAT_SABER_BUTTON_H
