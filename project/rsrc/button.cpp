#include "button.h"

Button::Button(float x, float y, float width, float height,
               sf::Font* font, std::string text,
               sf::Color idle_color, sf::Color hover_color, sf::Color active_color) {
    this->button_state = BTN_IDLE;

    this->button.setPosition(sf::Vector2f(x, y));
    this->button.setSize(sf::Vector2f(width, height));

    this->font = font;
    this->text.setFont(*this->font);
    this->text.setString(text);
    this->text.setFillColor(sf::Color(75, 0, 130));
    this->text.setCharacterSize(30);
    this->text.setPosition(
            this->button.getPosition().x + (button.getGlobalBounds().width / 2.f) - this->text.getGlobalBounds().width / 2.f,
            this->button.getPosition().y + (button.getGlobalBounds().height / 2.f) - this->text.getGlobalBounds().height / 2.f);

    this->idle_color = idle_color;
    this->hover_color = hover_color;
    this->active_color = active_color;
    this->button.setFillColor(this->idle_color);
}

Button::Button(float x, float y, float width, float height,
               sf::Color idle_color, sf::Color hover_color, sf::Color active_color) {
    this->button_state = BTN_IDLE;

    this->button.setPosition(sf::Vector2f(x, y));
    this->button.setSize(sf::Vector2f(width, height));


    this->idle_color = idle_color;
    this->hover_color = hover_color;
    this->active_color = active_color;
    this->button.setFillColor(this->idle_color);
    this->texture.loadFromFile("../project/src/pics/blackhole.png");
    this->button.setTexture(&texture);
    this->button.scale(1.9f, 1.9f);
}

const bool Button::is_pressed() const {
    return (this->button_state == BTN_ACTIVE);
}

void Button::render(sf::RenderWindow* window) {
    window->draw(this->button);
    window->draw(this->text);
}

void Button::update(const sf::Vector2f& mouse_pos) {
    this->button_state = BTN_IDLE;

    if (this->button.getGlobalBounds().contains(mouse_pos)) {
        this->button_state = BTN_HOVER;

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            this->button_state = BTN_ACTIVE;
        }
    }

    switch (this->button_state) {
        case BTN_IDLE:
            this->button.setFillColor(this->idle_color);
            break;

        case BTN_HOVER:
            this->button.setFillColor(this->hover_color);
            break;

        case BTN_ACTIVE:
            this->button.setFillColor(this->active_color);
            break;

        default:
            this->button.setFillColor(sf::Color::Red);

            break;
    }
}

void Button::update(bool cube_on_btn, int i) {
    this->button_state = BTN_IDLE;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::V) && i == 0 ||
        sf::Keyboard::isKeyPressed(sf::Keyboard::B) && i == 1 ||
        sf::Keyboard::isKeyPressed(sf::Keyboard::N) && i == 2 ||
        sf::Keyboard::isKeyPressed(sf::Keyboard::M) && i == 3) {
        this->button_state = BTN_ACTIVE;

        if (cube_on_btn) {
            this->button_state = BTN_CUBE;
        }
    }

    switch (this->button_state) {
        case BTN_IDLE:
            this->button.setFillColor(this->idle_color);
            break;

        case BTN_ACTIVE:
            this->button.setFillColor(this->hover_color);
            break;

        case BTN_CUBE:
            this->button.setFillColor(sf::Color::Green);
            break;

        default:
            this->button.setFillColor(sf::Color::Red);
            break;
    }
}
