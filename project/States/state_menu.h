#ifndef BEAT_SABER_STATE_MENU_H
#define BEAT_SABER_STATE_MENU_H

#include "state.h"
#include "../rsrc/button.h"
#include "state_game.h"
#include "state_download.h"

class State_Menu : public State {
private:
    std::vector<Button> btn;
    sf::Font font;
    sf::Text text;
    sf::Text text2;
    sf::Texture bg;
    sf::Sprite background;

    void init_fonts();
    void init_image();
public:
    State_Menu(sf::RenderWindow *window, std::stack<State*>* states);

    void check_quit_from_state(sf::Sound &sound, sf::Sound& sound1) override;
    void update(sf::Sound &sound, sf::Sound& sound1) override;
    void render() override;
};


#endif //BEAT_SABER_STATE_MENU_H

