#include "state_menu.h"

State_Menu::State_Menu(sf::RenderWindow *window, std::stack<State*>* states) : State(window, states) {
    window->setTitle("Menu");

    this->init_fonts();
    this->init_image();

    this->bg.setSmooth(true);
    this->background.setTexture(bg);
    this->background.scale(0.63f, 0.63f);

    this->text.setFont(this->font);
    this->text.setString("BEAT SABER 2D");
    this->text.setFillColor(sf::Color::White);
    this->text.setCharacterSize(50);
    this->text.setPosition( 720, 50);

    this->text2.setFont(this->font);
    this->text2.setString("SPACE EDITION");
    this->text2.setFillColor(sf::Color::White);
    this->text2.setCharacterSize(15);
    this->text2.setPosition( 860, 150);

    btn.push_back({850, 400, 160, 80, &this->font, "Start Game",
                   sf::Color::White,
                   sf::Color(176, 196, 222),
                   sf::Color(176, 196, 222)});

    btn.push_back({850, 550, 160, 80, &this->font, "Exit",
                   sf::Color::White,
                   sf::Color(176, 196, 222),
                   sf::Color(176, 196, 222)});

}

void State_Menu::update(sf::Sound &sound, sf::Sound& sound1) {
    // std::cout << "Menu\n";
    //sound.play();
    update_mouse_pos();

    for (auto& item : btn) {
        item.update(mouse_pos_view);
    }

    // std::cout << mouse_pos_view.x << " " << mouse_pos_view.y << "\n";
    check_quit_from_state(sound, sound1);
}

void State_Menu::render() {
    window->draw(background);
    window->draw(text);
    window->draw(text2);
    for (auto& item : btn) {
        item.render(window);
    }
}

void State_Menu::check_quit_from_state(sf::Sound &sound, sf::Sound& sound1) {
    if (btn[1].is_pressed()) {
        quit_game = true;
    }

    if (btn[0].is_pressed()) {
        states->push(new State_download(window,states));
    }

}

void State_Menu::init_fonts() {
    if (!this->font.loadFromFile("../padmaa.ttf")) {
        throw ("ERROR::COULDN'T LOAD FONT");
    }
}

void State_Menu::init_image() {
    if (!this->bg.loadFromFile("../project/src/pics/33879.jpg")) {
        throw ("ERROR::COULDN'T LOAD FONT");
    }
}
