#ifndef BEAT_SABER_STATE_GAME_H
#define BEAT_SABER_STATE_GAME_H

#include "state.h"
#include "../include/Object.h"
#include "../rsrc/button.h"
#include "state_menu.h"
#include "../Music/music.h"

class State_Game : public State {
private:
    CMusic mus;
    std::vector<Object*> cube[4];
    std::vector<Button*> buttons;

    sf::Texture back;
    sf::Sprite background;
    
    std::string filename[7] = {"1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "7.png"};

    sf::Clock clock;

    float timer;
    float time;
    int sample_per_second;
    int index;

    int index_per_second;
    int index_static;

    float time_in_music;
    float diff_times;
public:
    State_Game(sf::RenderWindow *window, std::stack<State*>* states);
    void check_quit_from_state(sf::Sound& sound, sf::Sound& sound1) override;
    void update(sf::Sound& sound, sf::Sound& sound1) override;
    void render() override;
    void update_time();
    void creating_of_cube();
    void update_music(sf::Sound& sound);

};

#endif //BEAT_SABER_STATE_GAME_H