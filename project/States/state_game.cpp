#include "state_game.h"
#include "../Music/music.h"

State_Game::State_Game(sf::RenderWindow *window, std::stack<State*>* states) : State(window, states) {
    window->setTitle("Game State");
    for (int i = 0; i < 4; i++) {
        auto* b = new Button(576 + 200 * i, 900, 80, 80,
                 sf::Color(100, 100, 255, 200),
                 sf::Color(100, 55, 255, 155),
                 sf::Color(100, 150, 255, 155));
        buttons.push_back(b);
    }
    timer = 0;
    diff_times = 0.0;
    index = 0;
    sample_per_second = mus.get_mag_spec().size() / mus.get_music_size();
    time_in_music = 0.0;
    index_static = 0;

    this->back.loadFromFile("../project/src/pics/background.jpg");
    this->back.setSmooth(true);
    this->background.setTexture(back);
    this->background.scale(0.63f, 0.63f);
    std::cout << mus.get_mag_spec().size() << " " << mus.get_music_size();
}

bool f = true;

void State_Game::update(sf::Sound &sound, sf::Sound &sound1) {
    update_time();

    update_music(sound);

//    sf::Time music_time = sound.getPlayingOffset();
//
//    while(mus.get_index_local_max()[index] / sample_per_second < static_cast<int>(music_time.asSeconds())) {
//        index++;
//    }
//
//
//    std::cout << music_time.asSeconds() << " " << mus.get_index_local_max()[index] / sample_per_second << "\n";
//    if (mus.get_index_local_max()[index] / sample_per_second == static_cast<int>(music_time.asSeconds())) {
//        creating_of_cube();
//        index++;
//    }

    this->update_mouse_pos();

    //update_mag_spec(sound);

    for(int i = 0; i < 4; i++) {
        if (cube[i].size()) {
            for (int j = 0; j < cube[i].size(); j++) {
                bool cube_on_btn = 0;

                if (buttons[i]->is_pressed() && cube[i][j]->get_position_y() > 860 && cube[i][j]->get_position_y() < 1080) {
                    cube_on_btn = 1;
                    cube[i].erase(cube[i].begin());
                }

                if (cube[i][j]->get_position_y() > 1080) {
                    cube[i].erase(cube[i].begin());
                }

                buttons[i]->update(cube_on_btn, i);
            }
        } else {
            buttons[i]->update(0, i);
        }
    }

    if (timer > 0.01) {
        for (int i = 0; i < 4; i++) {
            if (cube[i].size()) {
                for (int j = 0; j < cube[i].size(); j++) {
                    cube[i][j]->update(timer * 3);
                }
            }
        }
        timer = 0;
    }

    check_quit_from_state(sound, sound1);

    if (quit_state || sound1.getStatus() == sf::SoundSource::Stopped) {
        // delete states->top();
        states->push(new State_Menu(window,states));
    }
}

void State_Game::render() {
    window->draw(background);
    for (int i = 0; i < 4; i++) {
        if (cube[i].size()) {
            for (int j = 0; j < cube[i].size(); j++) {
                cube[i][j]->render(window);
            }
        }
        buttons[i]->render(window);
    }
}


void State_Game::check_quit_from_state(sf::Sound& sound, sf::Sound& sound1) {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        quit_state = true;
        sound1.stop();
        sound.stop();
        sf::SoundBuffer buffer;
        sound.resetBuffer();
        buffer.loadFromFile("../test.wav");
        sound.setBuffer(buffer);
    }
}

void State_Game::update_time() {
    time = clock.getElapsedTime().asSeconds();
    clock.restart();
    timer += time;
}

void State_Game::creating_of_cube() {
    int success = 0;
    while(1) {
        int i = rand() % 4;
        for (int j = 0; j < 4; j++) {
            if (j == i && ((cube[i].size()  == 0) || cube[i][cube[i].size() - 1]->get_position_y() > 90)) {
                Object* c = new Object(600+200*i, 0, filename[int(timer*100+i)%7]);
                cube[i].push_back(c);
                success++;
            }
        }
        if (success) {
            break;
        }
    }
}

void State_Game::update_music(sf::Sound& sound) {
    sf::Time music_time = sound.getPlayingOffset();

    if (music_time.asSeconds() == time_in_music) {
        return;
    }

    diff_times = music_time.asSeconds() - time_in_music;

    if (diff_times > 1) {
        index_per_second = sample_per_second / diff_times;
    } else {
        index_per_second = sample_per_second * diff_times;
    }

    while (mus.get_index_local_max()[index] < index_static) {
        index++;
    }

    if (mus.get_index_local_max()[index] > index_static &&
    mus.get_index_local_max()[index] < index_static + index_per_second) {
        creating_of_cube();
    }

    index_static += index_per_second;
    time_in_music = music_time.asSeconds();
}
