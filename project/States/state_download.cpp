#include "state_download.h"

State_download::State_download(sf::RenderWindow *window, std::stack<State *> *states) : State(window, states) {
    //window->setTitle("wait");
    this->back.loadFromFile("../project/src/pics/download_state.jpg");
    this->back.setSmooth(true);
    this->background.setTexture(back);
    this->background.scale(0.63f, 0.63f);
}

void State_download::check_quit_from_state(sf::Sound &sound, sf::Sound &sound1) { }

void State_download::update(sf::Sound &sound, sf::Sound &sound1) {

    this->text.setFont(this->font);
    if (!this->font.loadFromFile("../padmaa.ttf")) {
        throw ("ERROR::COULDN'T LOAD FONT");
    }
    this->text.setString("PLEASE WAIT...");
    this->text.setFillColor(sf::Color::White);
    this->text.setCharacterSize(50);
    this->text.setPosition( 700, 400);
    window->clear();
    window->draw(background);
    window->draw(text);
    window->display();
    states->push(new State_Game(window,states));

    //    sf::Time tin = sound.getPlayingOffset();
    //    int i = 0;

    sound.play();

    sound.setVolume(0);
    while (true){
//        window->draw(circle[i]);
        if (sound.getPlayingOffset().asSeconds() > 7.54) {
            sound1.play();
            break;
        }
    }
}

void State_download::render() {
    window->draw(background);
    window->draw(text);
}