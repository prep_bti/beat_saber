#ifndef BEAT_SABER_STATE_DOWNLOAD_H
#define BEAT_SABER_STATE_DOWNLOAD_H

#include "state.h"
#include "../rsrc/button.h"
#include "state_game.h"

class State_download : public State {
private:
    sf::Font font;
    sf::Text text;
    sf::Text text2;
    sf::Texture back;
    sf::Sprite background;
public:
    State_download(sf::RenderWindow *window, std::stack<State*>* states);

    void check_quit_from_state(sf::Sound &sound, sf::Sound& sound1) override;
    void update(sf::Sound &sound, sf::Sound& sound1) override;
    void render() override;
};

#endif //BEAT_SABER_STATE_DOWNLOAD_H
