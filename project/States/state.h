#ifndef BEAT_SABER_STATE_H
#define BEAT_SABER_STATE_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <stack>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>

class State {
private:
 //   std::vector<sf::Texture> textures;
protected:
    bool quit_game;
    bool quit_state;
    sf::RenderWindow* window;
    sf::Vector2i mouse_pos_screen;
    sf::Vector2i mouse_pos_window;
    sf::Vector2f mouse_pos_view;

    std::stack<State*>* states;
public:
    State(sf::RenderWindow*  window, std::stack<State*>* states);
    virtual ~State();

    bool get_quit_game() const;
    virtual void check_quit_from_state(sf::Sound &sound, sf::Sound& sound1) = 0;

    virtual void update_mouse_pos();
    virtual void update(sf::Sound &sound, sf::Sound& sound1) = 0;
    virtual void render() = 0;
};


#endif //BEAT_SABER_STATE_H
