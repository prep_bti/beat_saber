#include "state.h"

State::State(sf::RenderWindow *window, std::stack<State*>* states)
    : window(window), quit_state(false), quit_game(false), states(states) { }

State::~State() {

}

bool State::get_quit_game() const {
    return quit_game;
}

void State::update_mouse_pos() {
    mouse_pos_screen = sf::Mouse::getPosition();
    mouse_pos_window = sf::Mouse::getPosition(*window);
    mouse_pos_view = window->mapPixelToCoords(sf::Mouse::getPosition(*window));
}
