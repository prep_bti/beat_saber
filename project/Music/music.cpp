#include "music.h"

int CMusic::render() {
    sf::SoundBuffer buffer;
    sf::Sound sound;
    std::ifstream file_path ("../config.txt");

    if (!file_path) {
        throw ("ERROR::COULDN'T LOAD MUSIC PATH");
    }

    std::string s, str;

    getline(file_path,s);

    str = "../" + s;

    file_path.close();
    buffer.loadFromFile(str);
    sound.setBuffer(buffer);
    sound.play();
    //time - текущее время в песне, измеряется в микросекундах
    sf::Time time = sound.getPlayingOffset();
    return 0;
}

int count = 0;


int CMusic::update() {

    FFT();

    std::cout << "\nmax :: " << max;

    find_local_max();
    integration();
    return 0;
}

CMusic::CMusic() : max(DBL_MIN) {
    std::string music_path = "mono.wav";

    std::ifstream file_path ("../config.txt");

    if (!file_path) {
        throw ("ERROR::COULDN'T LOAD MUSIC PATH");
    }

    std::string s, str;

    getline(file_path,s);

    str = "../" + s;

    audio_file.load(str);

    file_path.close();

    music_duration = audio_file.getLengthInSeconds();

    audio_file.printSummary();

    audioFrameSize = 512;

    AudioFile<double>::AudioBuffer buffer;
    buffer.resize(2);

    buffer[0].resize(262144);
    buffer[1].resize(262144);

    audio_file.setSampleRate(262144/music_duration);

    int numChannels = 2;
    int numSamplesPerChannel = 262144;
    float sampleRate = audio_file.getSampleRate();
    float frequency = 440.f;

    for (int i = 0; i < numSamplesPerChannel; i++)
    {
        float sample = sinf (2. * M_PI * ((float) i / sampleRate) * frequency) ;

        for (int channel = 0; channel < numChannels; channel++)
            buffer[channel][i] = sample * 0.5;
    
    }

    audio_file.setAudioBuffer (buffer);

    magSpec.resize(audio_file.getNumSamplesPerChannel());

    audio_file.printSummary();

    //BTrack b;


    update();
}

void CMusic::FFT() {
    double audioFrameLeftChannel[audioFrameSize];
    std::vector<double> audioFrameRightChannel (audioFrameSize);

    int count = 0;
    for (int i = 0; i < audio_file.getNumSamplesPerChannel(); i += audioFrameSize)
    {
        for (int k = 0; k < audioFrameSize; k++)
        {
            audioFrameLeftChannel[k] = audio_file.samples[0][i + k];
            audioFrameRightChannel[k] = audio_file.samples[1][i + k];
        }

        FFTAnalysis(audioFrameLeftChannel, audioFrameSize, audioFrameSize, i);
    }
    
    for (int i = 0; i < magSpec.size(); i++) {
        for (int j = 0; j < magSpec[i] * 1000; j++) {
            std::cout << "-";
        }
        std::cout << std::endl;
    }
}


const double TwoPi = 6.283185307179586;


// aval - массив с музыкой (начальный)
void CMusic::FFTAnalysis(double *AVal, int Nvl, int Nft, int c) {
    int i, j, n, m, Mmax, Istp;
    double Tmpr, Tmpi, Wtmp, Theta;
    double Wpr, Wpi, Wr, Wi;
    double *Tmvl;

    n = Nvl * 2; Tmvl = new double[n];

    for (i = 0; i < n; i+=2) {
        Tmvl[i] = 0;
        Tmvl[i+1] = AVal[i/2];
    }

    i = 1; j = 1;
    while (i < n) {
        if (j > i) {
            Tmpr = Tmvl[i]; Tmvl[i] = Tmvl[j]; Tmvl[j] = Tmpr;
            Tmpr = Tmvl[i+1]; Tmvl[i+1] = Tmvl[j+1]; Tmvl[j+1] = Tmpr;
        }
        i = i + 2; m = Nvl;
        while ((m >= 2) && (j > m)) {
            j = j - m; m = m >> 1;
        }
        j = j + m;
    }

    Mmax = 2;
    while (n > Mmax) {
        Theta = -TwoPi / Mmax; Wpi = sin(Theta);
        Wtmp = sin(Theta / 2); Wpr = Wtmp * Wtmp * 2;
        Istp = Mmax * 2; Wr = 1; Wi = 0; m = 1;

        while (m < Mmax) {
            i = m; m = m + 2; Tmpr = Wr; Tmpi = Wi;
            Wr = Wr - Tmpr * Wpr - Tmpi * Wpi;
            Wi = Wi + Tmpr * Wpi - Tmpi * Wpr;

            while (i < n) {
                j = i + Mmax;
                Tmpr = Wr * Tmvl[j] - Wi * Tmvl[j-1];
                Tmpi = Wi * Tmvl[j] + Wr * Tmvl[j-1];

                Tmvl[j] = Tmvl[i] - Tmpr; Tmvl[j-1] = Tmvl[i-1] - Tmpi;
                Tmvl[i] = Tmvl[i] + Tmpr; Tmvl[i-1] = Tmvl[i-1] + Tmpi;
                i = i + Istp;
            }
        }

        Mmax = Istp;
    }
    for (i = 0; i < Nft; i++) {
        j = i * 2;
        magSpec[i+c] = 2 * sqrt(pow(Tmvl[j], 2) + pow(Tmvl[j + 1], 2)) / Nvl;
        if (magSpec[i+c] > max) {
            max = magSpec[i+c];
        }
    }

    delete []Tmvl;
}


void CMusic::integration(){
    for (int i = 0; i < audio_file.getNumSamplesPerChannel(); i += audio_file.getSampleRate()) {
        double max = magSpec[i];
        for (int j = i + 1; j < i + 512; j++) {
            if (magSpec[j] > max) {
                max = magSpec[j];
            }
        }
        rectangles.push_back(max);
    }
    std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    for (int i = 0; i < rectangles.size(); i ++) {
        std::cout << rectangles[i] << std::endl;
        /*for (int j = 0; j < rectangles[i] * 10000; j++) {
            std::cout << "-";
        }*/
        std::cout <<"end" <<std::endl;
    }
}


std::vector<double> CMusic::get_mag_spec() const {
    return magSpec;
}

int CMusic::get_max() const {
    return max;
}

int CMusic::get_music_size() const {
    return music_duration;
}

std::vector<int> CMusic::get_index_local_max() const {
    return index_local_max;
}

void CMusic::find_local_max() {
    for (int i = 0; i < magSpec.size(); ++i) {
        if (magSpec[i] > magSpec[i-1] && magSpec[i] > magSpec[i+1] && magSpec[i] > 0.495) {
            index_local_max.push_back(i);
        }
    }
}

