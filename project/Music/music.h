#ifndef BEAT_SABER_MUSIC_H
#define BEAT_SABER_MUSIC_H

#include <string>
#include <vector>
#include <unistd.h>
#include <iostream>
#include <utility>
#include <cfloat>
#include <fstream>
#include <cmath>

#include <SFML/Audio.hpp>

//#include "BTrack/src/BTrack.h"
//#include "libsamplerate-0.1.9/src/samplerate.h"
#include "AudioFile/AudioFile.h"
//#include "Gist/libs/kiss_fft130/kiss_fft.h"
//#include "Gist/src/Gist.h"

class CMusic {
private:
    AudioFile<double> audio_file;
    std::vector<double> magSpec;
    std::vector<double> rectangles;
    std::vector<int> index_local_max;
    int audioFrameSize;
    double max;

    int music_duration;

    int update();
    void FFTAnalysis(double *AVal, int Nvl, int Nft, int c);
    void FFT();
    void find_local_max();
    void integration();

public:
    CMusic();
    int render();
    std::vector<double> get_mag_spec() const;
    int get_max() const;
    int get_music_size() const;
    std::vector<int> get_index_local_max() const;
};

#endif //BEAT_SABER_MUSIC_H
