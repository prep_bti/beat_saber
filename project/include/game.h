#ifndef BEAT_SABER_GAME_H
#define BEAT_SABER_GAME_H

#include "../States/state_game.h"
#include "../States/state_menu.h"
#include <SFML/Graphics.hpp>

class Game {
private:
    sf::RenderWindow* window;

    sf::Event sf_event;
    std::stack<State*> states;

    sf::Sound sound;
    sf::Sound sound_true;

    void init_window();
    void init_states();
public:
    Game();
    ~Game();

    // Functions
    void update();
    void render();
    void update_SFML_events();
    void run();
};


#endif //BEAT_SABER_GAME_H
