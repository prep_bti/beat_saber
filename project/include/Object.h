#ifndef BEAT_SABER_OBJECT_H
#define BEAT_SABER_OBJECT_H

#include <SFML/Graphics.hpp>
#include <string>

// пофиг на заднем фоне, должны лететь под кнопками ???
class Object {
private:
   sf::RectangleShape rectangle;
   sf::Texture texture;
   sf::Sprite sprite;
   // int speed;
public:
   Object(int x, int y, std::string filename);
   ~Object() = default;
   void init_texture(std::string filename);
   void init_sprite(int x, int y);
   void update(float timer); // move
   void render(sf::RenderWindow* window); // рисовать кубик
   int get_position_y();
   void set_position(int x, int y);
   //stop_move(); // остановка Оля
};


#endif //BEAT_SABER_OBJECT_H

